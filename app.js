var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api/*', require('./app/middleware/authorize'));
app.use('/auth', require('./app/routes/auth/signin'));
app.use('/user', require('./app/routes/user/create-user'));
app.use('/user', require('./app/routes/user/activate-user'));
app.use('/api/message', require('./app/routes/api/message/create-message'));
app.use('/api/message', require('./app/routes/api/message/get-messages'));

app.listen(8080, function() {
    console.log('Server listening on port 8080');
});

//TODO: pgDone must be fixed!!!!

//bluebeamapps.chatapp.test@gmail.com
//qweasd1234
//https://www.getpostman.com/collections/330e0ab05d628f5b5e37