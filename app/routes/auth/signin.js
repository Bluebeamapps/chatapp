var express = require('express');
var router = express.Router();
var async = require('async');
var pg = require('pg');
var bcrypt = require('bcrypt');
var uuid = require('uuid');
var jwt = require('jsonwebtoken');
var serverResponse = require('../../helpers/response');
var secretKey = require('../../secret').secretKey;
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

var unauthorizedMessage = 'The email address or password you entered is not valid';
var pgDone = null;

router.post('/signin', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        findUserInDatabase,
        comparePassword
    ], 
    function(result) {
        if (pgDone) pgDone();
        serverResponse.json(result, response);
    });
})

function validateJSON(request, next) {
    var json = request.body;
    
    var isValid = json &&
        json.hasOwnProperty('email') &&
        json.hasOwnProperty('password');
        
    var err;
        
    if (!isValid) {
        err = serverResponse.badRequestError('Invalid JSON');
    } else if (json.email.length == 0) {
        err = serverResponse.badRequestError('Email must not be empty');
    } else if (json.password.length == 0) {
        err = serverResponse.badRequestError('Password must not be empty');
    }

    next(err, json);
}

function findUserInDatabase(json, next) {
    pg.connect(connectionString, function(err, client, done) {
        pgDone = done;
        
        if (err) {
            return next(err);
        }
        
        var query = 'SELECT Activated, Password FROM MessageDb.Users WHERE Email = $1';
        var values = [json.email];
        
        client.query(query, values, function(err, result) {
            if (err) {
                return next(err);
            }
            
            var user;
            
            if (result.rows.length > 0) {
                user = result.rows[0];
            } 
            
            if (user) {
                if (!user.activated) {
                    next(serverResponse.unauthorizedError('User account has not been activated yet.', 1));
                } else {
                    next(err, json, user);
                }
            } else {
                next(serverResponse.unauthorizedError(unauthorizedMessage));
            }
        });
    });
}

function comparePassword(json, user, next) {
    bcrypt.compare(json.password, user.password, function(err, same) {
        if (err) {
            next(err);
        }
        
        if (same) {
            var token = generateToken(user);
            
            var responseObject = {
                message: 'User successfully authenticated.',
                data: {authToken: token}
            };
            console.log(pgDone);
            next(responseObject);
        } else {
            next(serverResponse.unauthorizedError(unauthorizedMessage));
        }
    });
}

function generateToken(user) {
    var token = jwt.sign({
            sub: user.email, 
            uId: user.id
        }, 
        secretKey, {expiresIn: 1800});
        
    return token;
}

module.exports = router;