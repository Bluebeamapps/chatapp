var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../../helpers/response');
var connectionString = require('../../../../app-config').DB_CONNECTION_URL;

var pgDone = null;

router.post('create', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request)
    ], 
    function(err) {
        sendResponse(err, response);
    });
});

function validateJSON(request, err, next) {
    var json = request.body;
    
    var isValid = json &&
        json.hasOwnProperty('userIDTarget') &&
        json.hasOwnProperty('userIDSender') &&
        json.hasOwnProperty('chatID') &&
        json.hasOwnProperty('text');
        
    var err;
    
    if (!isValid) {
        err = new Error('Invalid JSON');
    }
    
    next(err, json);
}

function createChat(json, next) {
    if (json.chatID != 0) {
        return next(null, json);    
    }
    
    pg.connect(connectionString, function(err, client, done) {
        pgDone = done;
        
        if (err) {
            return next(err);
        }
        
        var query = 'INSERT INTO MessageDb.Chats VALUES (DEFAULT, now()) RETURNING Id;';
        
        client.query(query, function(err, result) {
            if (err) {
                return next(err);
            }
            
            json.chatID = result.rows[0].id;
            
            var query = 'INSERT INTO MessageDb.ChatUsers VALUES ($1, $2), ($1, $3);';
            var values = [json.chatID, json.userIDTarget, json.userIDSender];
            
            client.query(query, values, function(err, client) {
                next(err, json, client);
            });
        });
    });
}

function createMessage(json, client, next) {
    var query = 'INSERT INTO MessageDb.Messages VALUES (DEFAULT, $1, $2, $3);';
    var values = [json.userIDSender, json.chatID, json.text];
    
    client.query(query, values, function(err, result) {
        next(err);
    });
}

function sendResponse(err, response) {
    if (pgDone) {
        pgDone();
    }
    
    serverResponse.json(err, response);
}

module.exports = router;