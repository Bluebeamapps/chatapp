var express = require('express');
var async = require('async');
var pg = require('pg');
var serverResponse = require('../../../helpers/response');
var connectionString = require('../../../../app-config').DB_CONNECTION_URL;

var router = express.Router();

router.get('', function(request, response) {
    async.waterfall([
        async.apply(validateRequest, request)
    ],
    function(result) {
        serverResponse.json(result, response)
    });
});

function validateRequest(request, next) {
    var params = request.body;
    
    var isValid = params &&
        params.hasOwnProperty('chatId');
        
    var err;
        
    if (!isValid) {
        err = new Error('Invalid JSON.');
    }
    
    next(err, params);
}

function retrieveMessagesFromDatabase(params, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            return next(err);
        }
        
        var query = 'SELECT * FROM MessageDb.ChatUsers WHERE ChatId = $1';
        var values = [json.chatId];
        
        client.query(query, values, function(err, result) {
            if (err) {
                return next(err);
            }
            
            query = 'SELECT * FROM MessageDb.Messages WHERE ChatId = $1 AND UserId = $2 OR UserId = $3;'
            values = [json.chatId, result.rows[0].useridfk, result.rows[1].useridfk];
            
            client.query(query, values, function(err, result) {
                if (err) {
                    return next(err);
                }
                
                var responseObject = {
                    data: result.rows
                }
                
                next(responseObject);
            });
        });
    });
}

module.exports = router;