var express = require('express');
var async = require('async');
var pg = require('pg');
var serverResponse = require('../../helpers/response');
var router = express.Router();
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.get('/activate', function(request, response) {
    async.waterfall([
        async.apply(validateRequest, request),
        activateUser
    ], 
    function(err, results) {
        if (err) {
            console.error('An error occurred: ', err);
            
            var status = err.status || 500;
            
            //TODO: Render an html informing the user about the error. Do NOT print a JSON.
            response.status(status).send();
        } else {
            response.status(200).send();
        }
    });
});

function validateRequest(request, next) {
    var isValid = request.query &&
        request.query.code &&
        request.query.email;
        
    var err;
        
    if (!isValid) {
        err = serverResponse.badRequestError('Invalid request parameters.');
    } else if (request.query.code.length == 0) {
        err = serverResponse.badRequestError('An activation code must be provided.');
    } else if (request.query.email.length == 0) {
        err = serverResponse.badRequestError('An email must be provided.');
    }
    
    next(err, request);
}

function activateUser(request, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            return next(err);
        }
        
        var query = 'UPDATE MessageDb.Users SET Activated = TRUE WHERE Email = $1 AND ActivationCode = $2';
        var values = [request.query.email, request.query.code];
        
        client.query(query, values, function(err, result) {
            if (err) {
                return next(err);
            }
            
            done();
            next(err);
        });
    });
}

module.exports = router;