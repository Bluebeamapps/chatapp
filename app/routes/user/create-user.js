var async = require('async');
var express = require('express');
var pg = require('pg');
var shortid = require('shortid');
var bcrypt = require('bcrypt');
var mail = require('../../helpers/mail');
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

var router = express.Router();
var pgDone = null;

router.post('/signup', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        findUserInDatabase,
        hashPassword,
        createNewUser,
        sendMail
    ], 
    function(result) {
        if (pgDone) pgDone();
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var json = request.body;
    
    var isValid = json &&
        json.hasOwnProperty('name') && 
        json.hasOwnProperty('email') &&
        json.hasOwnProperty('password');
    
    var err;
    
    if (!isValid) {
        err = serverResponse.badRequestError('Invalid JSON.');
    } else if (json.name.length == 0) {
        err =  serverResponse.badRequestError('Invalid name');
    } else if (json.email.length == 0) {
        err = serverResponse.badRequestError('Invalid email');
    } else if (json.password.length == 0) {
        err = serverResponse.badRequestError('Invalid password');
    }

    next(err, json); 
}

function findUserInDatabase(json, next) {
    pg.connect(connectionString, function(err, client, done) {
        pgDone = done;
        
        if (err) {
            return next(err);
        }

        var query = 'SELECT count(*) FROM MessageDb.Users WHERE Email = $1';
        
        client.query(query, [json.email], function(err, result) {
            if (err) {
                return next(err);
            }
            
            if (result.rows[0].count == 0) {
                //Insert new user.
                next(err, json, client);
            } else {
                err = serverResponse.badRequestError('A user with this email address already exists');
                next(err);
            }
        });
    });
}

function hashPassword(json, client, next) {
    bcrypt.genSalt(8, function(err, salt) {
        if (err) {
            next(err);
        }
        
        bcrypt.hash(json.password, salt, function(err, encrypted) {
            next(err, client, encrypted, json);
        });
    });
}

function createNewUser(client, encryptedPassword, json, next) {
    var activationCode = shortid.generate();
    
    var query = 'INSERT INTO MessageDb.Users VALUES (DEFAULT, $1, $2, $3, $4, FALSE, now()) RETURNING *;';
    var values = [json.name, json.email, encryptedPassword, activationCode];
    
    client.query(query, values, function(err, result) {
        next(err, result.rows[0]);
    });
}

function sendMail(user, next) {
    mail.verificationEmail(user, function() {
        next({message: 'User successfully created.'});
    });
}

module.exports = router;