CREATE SCHEMA MessageDb;

CREATE TABLE MessageDb.Users (
  Id serial PRIMARY KEY, 
  Name text,
  Email text,
  Password text,
  ActivationCode text,
  Activated boolean,
  CreatedAt timestamptz
);

CREATE TABLE MessageDb.Chats (
  Id serial PRIMARY KEY,
  CreatedAt timestamptz
);

CREATE TABLE MessageDb.Messages (
  Id serial PRIMARY KEY, 
  UserId int, 
  ChatId int, 
  Content text, 
  FOREIGN KEY (UserId) REFERENCES MessageDb.Users(Id), 
  FOREIGN KEY (ChatId) REFERENCES MessageDb.Chats(Id)
);

CREATE TABLE MessageDb.ChatUsers (
  ChatIdFk int,
  UserIdFk int,
  FOREIGN KEY (UserIdFk) REFERENCES MessageDb.Users(Id),
  FOREIGN KEY (ChatIdFK) REFERENCES MessageDb.Chats(Id)
);