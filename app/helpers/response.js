module.exports = {
    json: function(responseObject, response) {
        if (responseObject && responseObject.stack) {
            //Unhandled(generic) error.
            console.error('An error occurred: ', responseObject);
            response.status(500).json({
                message: 'An internal error occurred.'
            });
        } else {
            var status = responseObject ? responseObject.status || 200 : 200;
            
            var json = {};
            
            json.errorCode = responseObject ? responseObject.errorCode || undefined : undefined;
            json.message = responseObject ? responseObject.message || undefined : undefined;
            json.data = responseObject ? responseObject.data || undefined : undefined;
            
            response.status(status).json(json);
        }
    },
    
    badRequestError: function(message, code) {
        return {message: message, status: 400, errorCode: code || 0};
    },
    
    unauthorizedError: function(message, code) {
        return {message: message, status: 401, errorCode: code || 0};
    },
    
    forbiddenError: function(message, code) {
        return {message: message || 'Access denied.', status: 403, errorCode: code || 0};
    },
    
    notFoundError: function(message, code) {
        return {message: message || 'Not found.', status: 404, errorCode: code || 0};
    },
    
    tooManyRequestsError: function(message, code) {
        return {message: message, status: 429, errorCode: code || 0};
    }
}