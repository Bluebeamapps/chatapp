var jwt = require('jsonwebtoken');
var async = require('async');
var serverResponse = require('../helpers/response');
var secretKey = require('../secret').secretKey;

module.exports = function(request, response, next) {
    async.waterfall([
        async.apply(validateRequest, request),
        verifyToken
    ], 
    function(err) {
        if (err) {
            serverResponse.json(err, response);
        } else {
            next();//Next middleware.
        }
    });
}

function validateRequest(request, next) {
    var err;
    
    if (!request.headers || !request.headers.authorization) {
        err = serverResponse.unauthorizedError('No authorization header or token was found in the request.');
    }
    
    next(err, request);
}

function verifyToken(request, next) {
    var token = request.headers.authorization;
    
    jwt.verify(token, secretKey, function(err, decoded) {
        if (err && err.name === 'TokenExpiredError') {
            err = serverResponse.unauthorizedError('Invalid credentials.');
        }
        
        request.user = {userId: decodedToken.uId, accessLevel: decodedToken.accessLevel};

        next(err);
    });
}