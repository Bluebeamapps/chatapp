module.exports = (function() {
    var devSettings = {
        DB_CONNECTION_URL: 'postgres://localhost:5432/messagedb'
    };
    
    var productionSettings = {
        DB_CONNECTION_URL: 'postgres://localhost:5432/messagedb'
    };

    switch(process.env.NODE_ENV) {
        case 'development': {
            console.log('dev');
            return devSettings;
        }


        case 'production': {
            console.log('prod');
            return productionSettings;
        }

        default:
            return {};
    }
})();